@extends('layout.app')

@section('content')
<h1>Messages</h1>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos molestias corporis veritatis? Repellendus asperiores iste autem atque sit quidem dignissimos inventore architecto ratione velit, error ab dicta officia similique dolore.
    Lorem ipsum dolor sit amet consectetur adipisicing elit. 
</p>
@if(count($messages) > 0)
@foreach($messages as $message)
<p>
    <ul class="list-group text-muted">
        <li class="list-group-item">
        Name : {{$message->name}}
        </li>
        <li class="list-group-item">
        Email : {{$message->email}}
        </li>
        <li class="list-group-item">
        Subject : {{$message->subject}}
        </li>
        <li class="list-group-item">
        Message : {{$message->message}}
        </li>
    </ul>
</p>
@endforeach
@endif

@endsection

@section('sidebar')
@parent
<p>This is apended to the sidebar</p>
@endsection