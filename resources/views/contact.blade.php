@extends('layout.app')

@section('content')
<h1>Contact</h1>

<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Animi voluptates sit odit doloribus! Quia, velit nihil, aperiam officiis error earum impedit quis fuga, voluptas veniam reiciendis? Sunt odit provident possimus. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nesciunt rerum mollitia unde officiis amet facere voluptatem magni porro, natus illo quibusdam nulla provident possimus distinctio quasi eveniet numquam ullam laborum?</p>

{!! Form::open() !!}
<div class="form-group">
    {{Form::label('name', 'Name', ['class' => 'sr-only'])}}
    {{Form::text('name', '', ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Your Name'])}}
    
    @if($errors->has('name'))
    <div class="alert alert-danger">
    {{$errors->first('name')}}
    </div>
    @endif
</div>
<div class="form-group">
    {{Form::label('email', 'Email', ['class' => 'sr-only'])}}
    {{Form::email('email', '', ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Your Email'])}}

    @if($errors->has('email'))
    <div class="alert alert-danger">
    {{$errors->first('email')}}
    </div>
    @endif
</div>
<div class="form-group">
    {{Form::label('subject', 'Subject', ['class' => 'sr-only'])}}
    {{Form::text('subject', '', ['class' => 'form-control', 'id' => 'subject', 'placeholder' => 'Subject'])}}

    @if($errors->has('subject'))
    <div class="alert alert-danger">
    {{$errors->first('subject')}}
    </div>
    @endif
</div>
<div class="form-group">
    {{Form::label('message', 'Message', ['class' => 'sr-only'])}}
    {{Form::textarea('message', '', ['class' => 'form-control', 'id' => 'message', 'placeholder' => 'Your Message', 'rows' => 4])}}

    @if($errors->has('message'))
    <div class="alert alert-danger">
    {{$errors->first('message')}}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-5 col-lg-5 mb-2">
        {{Form::submit('submit', ['class' => 'btn btn-primary btn-block'])}}
    </div>
</div>
{!! Form::close() !!}

@endsection