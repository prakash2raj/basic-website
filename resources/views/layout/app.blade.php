<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acme</title>
    
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
    @include('inc.navbar')

    <div class="container">
        @if(Request::is('/'))
        @include('inc.showcase')
        @endif
        <div class="row">
            <div class="col-md-8 col-lg-8">
                @include('inc.messages')
                
                @yield('content')
            </div>
            <div class="col-md-4 col-lg-4">
                @include('inc.sidebar')
            </div>
        </div>
    </div>

    <footer id="footer" class="text-center text-light bg-dark py-3 mt-3">
        <p class="pt-2">Copyright &copy {{date('Y')}} | Developed by <a id="prakash" href="https://about.techprakash.com" target="_blank" title="Web Developer">Prakash Raj</a> | <span id="powerBy">Powered By Laravel</span></p>
    </footer>

</body>
<script src="/js/app.js"></script>

</html>