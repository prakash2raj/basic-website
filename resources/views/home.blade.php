@extends('layout.app')

@section('content')
<h1>Home</h1>
<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos molestias corporis veritatis? Repellendus asperiores iste autem atque sit quidem dignissimos inventore architecto ratione velit, error ab dicta officia similique dolore.
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum repellendus pariatur omnis officiis distinctio repellat obcaecati est fugit nemo sint beatae magnam, earum inventore deleniti corrupti fuga vitae possimus unde?
    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Amet omnis accusantium asperiores iusto suscipit dignissimos dolor molestias animi ipsa officiis, repudiandae facilis repellat voluptates accusamus ipsam quisquam nemo non iste.
</p>

@endsection

@section('sidebar')
@parent
<p>This is apended to the sidebar</p>
@endsection